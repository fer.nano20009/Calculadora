package p3_tp1;

import java.awt.Button;
import java.awt.EventQueue;
import java.awt.List;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Color;

public class CalculadoraGrafica  implements MouseListener
{

	private JFrame frame;
	private JTextField Expresion;
	
	private char SUMA='+';
	private char RESTA='-';
	private char DIVISION='/';
	private char MULTIPLICACION='X';
	private ArrayList<Character> OperandoresSeleccionados;
	private ArrayList<Character> OperandoresUtilizados;
	private JButton boton_1,boton_2,boton_3,boton_4,boton_5,boton_6,boton_7,boton_8,boton_9,boton_0,boton_suma,boton_resta,boton_multiplicacion,boton_division,boton_enviar;
	private CalculadoraLogica calculadoraLogica;
	private JTextField Numero_Azar;
	private JTextField Operador_azar_1;
	private JTextField Operador_azar_2;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() {
				try {
					CalculadoraGrafica window = new CalculadoraGrafica();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CalculadoraGrafica() 
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	

	private void initialize()
	{
		this.OperandoresSeleccionados=new ArrayList<Character>();
		this.OperandoresUtilizados=new ArrayList<>();
		
		this.calculadoraLogica=new CalculadoraLogica();
		frame = new JFrame();
		frame.setBounds(100, 100, 400, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		// No numeros
		Inicializar_botones_operandos();
		
		
		
		//Numeros
		
		Inicializar_botones_numeros();
		
		
		Expresion = new JTextField();
		Expresion.setHorizontalAlignment(SwingConstants.RIGHT);
		Expresion.setEnabled(false);
		Expresion.setEditable(false);
		Expresion.setBounds(10, 147, 312, 39);
		frame.getContentPane().add(Expresion);
		Expresion.setColumns(10);
		
		
		
		// Se eligen al azar el numero y los operadores
		
		
		
		
		inicializar_elementos_azar();
		GenerarOperandoresAleatorios(Numero_Azar,Operador_azar_1,Operador_azar_2);
		
		Activar_botones();
	}

	private void GenerarOperandoresAleatorios(JTextField numero_a_resolver, JTextField operador_azar_1, JTextField operador_azar_2) 
	{
		Random random=new Random();
		int numeroAlAzar=random.nextInt(1000);
		numero_a_resolver.setText(Integer.toString(numeroAlAzar));
		
		int numeroAleatorio_1=random.nextInt(4);
		int numeroAleatorio_2=random.nextInt(4);
		
		if (numeroAleatorio_1==0)
		{
			this.OperandoresSeleccionados.add(SUMA);
		}
		else if (numeroAleatorio_1==1)
		{
			this.OperandoresSeleccionados.add(RESTA);
		}
		else if (numeroAleatorio_1==2)
		{
			this.OperandoresSeleccionados.add(DIVISION);
		}
		else if (numeroAleatorio_1==3)
		{
			this.OperandoresSeleccionados.add(MULTIPLICACION);
		}
		
		if (numeroAleatorio_2==0)
		{
			this.OperandoresSeleccionados.add(SUMA);
		}
		else if (numeroAleatorio_2==1)
		{
			this.OperandoresSeleccionados.add(RESTA);
		}
		else if (numeroAleatorio_2==2)
		{
			this.OperandoresSeleccionados.add(DIVISION);
		}
		else if (numeroAleatorio_2==3)
		{
			this.OperandoresSeleccionados.add(MULTIPLICACION);
		}
		operador_azar_1.setText(this.OperandoresSeleccionados.get(0).toString());
		operador_azar_2.setText(this.OperandoresSeleccionados.get(1).toString());
		
	}

	private void inicializar_elementos_azar() {
		Numero_Azar = new JTextField();
		Numero_Azar.setEditable(false);
		Numero_Azar.setHorizontalAlignment(SwingConstants.CENTER);
		Numero_Azar.setColumns(10);
		Numero_Azar.setBounds(16, 11, 64, 57);
		frame.getContentPane().add(Numero_Azar);
		
		Operador_azar_1 = new JTextField();
		Operador_azar_1.setBackground(Color.PINK);
		Operador_azar_1.setEditable(false);
		Operador_azar_1.setHorizontalAlignment(SwingConstants.CENTER);
		Operador_azar_1.setBounds(83, 11, 64, 57);
		frame.getContentPane().add(Operador_azar_1);
		Operador_azar_1.setColumns(10);
		
		
		Operador_azar_2 = new JTextField();
		Operador_azar_2.setBackground(Color.PINK);
		Operador_azar_2.setEditable(false);
		Operador_azar_2.setHorizontalAlignment(SwingConstants.CENTER);
		Operador_azar_2.setColumns(10);
		Operador_azar_2.setBounds(153, 11, 64, 57);
		
		frame.getContentPane().add(Operador_azar_2);
	}

	private void Activar_botones() {
		boton_1.addMouseListener(this);
		boton_2.addMouseListener(this);
		boton_3.addMouseListener(this);
		boton_4.addMouseListener(this);
		boton_5.addMouseListener(this);
		boton_6.addMouseListener(this);
		boton_7.addMouseListener(this);
		boton_8.addMouseListener(this);
		boton_9.addMouseListener(this);
		boton_0.addMouseListener(this);
		
		boton_suma.addMouseListener(this);
		boton_resta.addMouseListener(this);
		boton_division.addMouseListener(this);
		boton_multiplicacion.addMouseListener(this);		
		boton_enviar.addMouseListener(this);
	}

	private void Inicializar_botones_numeros() {
		this.boton_1 = new JButton("1");
		boton_1.setBounds(14, 203, 44, 23);
		frame.getContentPane().add(boton_1);
		
		this.boton_2 = new JButton("2");
		boton_2.setBounds(78, 203, 44, 23);
		frame.getContentPane().add(boton_2);
		
		this.boton_3 = new JButton("3");
		boton_3.setBounds(142, 203, 44, 23);
		frame.getContentPane().add(boton_3);
		
		this.boton_4 = new JButton("4");
		boton_4.setBounds(14, 237, 44, 23);
		frame.getContentPane().add(boton_4);
		
		this.boton_5 = new JButton("5");
		boton_5.setBounds(77, 237, 44, 23);
		frame.getContentPane().add(boton_5);
		
		this.boton_6 = new JButton("6");
		boton_6.setBounds(142, 237, 44, 23);
		frame.getContentPane().add(boton_6);
		
		this.boton_7 = new JButton("7");
		boton_7.setBounds(14, 271, 44, 23);
		frame.getContentPane().add(boton_7);
		
		this.boton_8 = new JButton("8");
		boton_8.setBounds(77, 271, 44, 23);
		frame.getContentPane().add(boton_8);
		
		this.boton_9 = new JButton("9");
		boton_9.setBounds(142, 271, 44, 23);
		frame.getContentPane().add(boton_9);
		
		this.boton_0 = new JButton("0");
		boton_0.setBounds(78, 305, 44, 23);
		frame.getContentPane().add(boton_0);
	}

	private void Inicializar_botones_operandos() {
		this.boton_suma = new JButton("+");		
		boton_suma.setBounds(196, 305, 89, 23);
		frame.getContentPane().add(boton_suma);
		
		this.boton_resta = new JButton("-");
		boton_resta.setBounds(196, 203, 89, 23);
		frame.getContentPane().add(boton_resta);
		
		this.boton_division = new JButton("/");
		boton_division.setBounds(196, 237, 89, 23);
		frame.getContentPane().add(boton_division);
		
		this.boton_multiplicacion = new JButton("X");
		boton_multiplicacion.setBounds(196, 271, 89, 23);
		frame.getContentPane().add(boton_multiplicacion);
		
		this.boton_enviar = new JButton("Enviar");
		boton_enviar.setBounds(196, 339, 89, 23);
		frame.getContentPane().add(boton_enviar);
	}

	private boolean verificarExpresion(char c, JTextField expresion) 
	{
		if (expresion.getText().isEmpty())
		{
			if (c==RESTA)
			{
				return true;
			}
			else
			{
				return false;
			}
			
		}
		else if ( esOperador(expresion.getText().charAt(expresion.getText().length()-1)))
		{
			return false;
		}
		if (!this.OperandoresSeleccionados.contains(c))
		{
			return false;
		}
		if (this.OperandoresUtilizados.contains(c))
		{
			return false;
		}
		
				
		return true;
	}

	private boolean esOperador(char caracter) 
	{
		if (caracter==SUMA)
			return true;
		else if (caracter==RESTA)
			return true;
		else if (caracter==DIVISION)
			return true;		
		else if (caracter==MULTIPLICACION)
			return true;		
		else
			return false;
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
	
		
		if (e.getSource() == boton_1)
			Expresion.setText(Expresion.getText()+boton_1.getText());
		else if (e.getSource() == boton_2)
			Expresion.setText(Expresion.getText()+boton_2.getText());
		else if (e.getSource() == boton_3)
			Expresion.setText(Expresion.getText()+boton_3.getText());
		else if (e.getSource() == boton_4)
			Expresion.setText(Expresion.getText()+boton_4.getText());
		else if (e.getSource() == boton_5)
			Expresion.setText(Expresion.getText()+boton_5.getText());
		else if (e.getSource() == boton_6)
			Expresion.setText(Expresion.getText()+boton_6.getText());
		else if (e.getSource() == boton_7)
			Expresion.setText(Expresion.getText()+boton_7.getText());
		else if (e.getSource() == boton_8)
			Expresion.setText(Expresion.getText()+boton_8.getText());
		else if (e.getSource() == boton_9)
			Expresion.setText(Expresion.getText()+boton_9.getText());
		else if (e.getSource() == boton_0)
			Expresion.setText(Expresion.getText()+boton_0.getText());
		
		else if (e.getSource() == boton_suma)		
			{
				if (verificarExpresion(SUMA,Expresion))
					{
						Expresion.setText(Expresion.getText()+boton_suma.getText());
						this.OperandoresUtilizados.add(SUMA);
						 
						this.boton_suma.setEnabled(false);
					}
			}			
		else if (e.getSource() == boton_resta)
		{
			if (verificarExpresion(RESTA,Expresion))
				{
					Expresion.setText(Expresion.getText()+boton_resta.getText());		
					this.OperandoresUtilizados.add(RESTA);
					
					this.boton_resta.setEnabled(false);
				}
		}
			
		else if (e.getSource() == boton_division)
		{
			if (verificarExpresion(DIVISION,Expresion))
				{
					Expresion.setText(Expresion.getText()+boton_division.getText());	
					this.OperandoresUtilizados.add(DIVISION);
					
					this.boton_division.setEnabled(false);
				}
		}
			
		else if (e.getSource() == boton_multiplicacion)
		{
			if (verificarExpresion(MULTIPLICACION,Expresion))
			{
					Expresion.setText(Expresion.getText()+boton_multiplicacion.getText());	
					this.OperandoresUtilizados.add(MULTIPLICACION);
					
					this.boton_multiplicacion.setEnabled(false);
			}
								
		}		
		else if (e.getSource() == boton_enviar)
			{
			
				
				if (verificarEnviar(Expresion.getText()))
				{
					
					String resultado=String.valueOf(calculadoraLogica.Resultado(Expresion.getText()));				
					Expresion.setText(""+resultado);
				}
			}
						
	}

	private boolean verificarEnviar(String text)
	{
		
		int cant_operadores_usados=0;
		for (int i = 0; i < text.length(); i++) 
		{
			if (esOperador(text.charAt(i)))
				cant_operadores_usados++;
		}
		
		if (cant_operadores_usados!=this.OperandoresSeleccionados.size())
					return false;			
		else
			return true;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method sttub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
